import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let product: Product[] = [
  { id: 1, name: 'Chocolate', price: 25.5 },
  { id: 2, name: 'Green Tea', price: 55.0 },
  { id: 3, name: 'Strawberry', price: 20.0 },
];

let lastProductId = 4;
@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto,
    };
    product.push(newProduct);
    return newProduct;
  }

  findAll() {
    return product;
  }

  findOne(id: number) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return product[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...product[index],
      ...updateProductDto,
    };
    product[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProduct = product[index];
    product.splice(index, 1);
    return deletedProduct;
  }

  reset() {
    product = [
      { id: 1, name: 'Chocolate', price: 25.5 },
      { id: 2, name: 'Green Tea', price: 55.0 },
      { id: 3, name: 'Strawberry', price: 20.0 },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
