import { IsNotEmpty, MinLength, Matches, MATCHES } from 'class-validator';
export class CreateUserDto {
  @IsNotEmpty()
  @MinLength(5)
  login: string;
  @IsNotEmpty()
  @MinLength(5)
  name: string;
  @IsNotEmpty()
  @MinLength(8)
  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  password: string;
}
